package com.win.testcase;

import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.util.Log;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressBack;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.ComponentNameMatchers.hasClassName;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Winardi on 8/4/17.
 */

public class MainActivityTest {
    @Rule
    public ActivityTestRule<RegisterActivity> mActivity = new ActivityTestRule<>(RegisterActivity.class);

    @Test
    public void login() {
//REGISTER
        //INPUT FIRST NAME
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(withText("Register")));
        onView(withId(R.id.etFirstName)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("Hi"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        onView(withId(R.id.etFirstName)).check(ViewAssertions.matches(isDisplayed())).perform(clearText());
        onView(withId(R.id.etFirstName)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("Cat"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        //INPUT LAST NAME
        onView(withId(R.id.etLastName)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("Yo"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        onView(withId(R.id.etLastName)).check(ViewAssertions.matches(isDisplayed())).perform(clearText());
        onView(withId(R.id.etLastName)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("Dog"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        //INPUT EMAIL
        onView(withId(R.id.etEmail)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("test"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        onView(withId(R.id.etEmail)).check(ViewAssertions.matches(isDisplayed())).perform(clearText());
        onView(withId(R.id.etEmail)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("admin@admin.com"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        //INPUT PASSWORD
        onView(withId(R.id.etPassword)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("test"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        onView(withId(R.id.etPassword)).check(ViewAssertions.matches(isDisplayed())).perform(clearText());
        onView(withId(R.id.etPassword)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("admin"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        //INPUT CONFIRM PASSWORD
        onView(withId(R.id.etConfirmPassword)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("test"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        onView(withId(R.id.etConfirmPassword)).check(ViewAssertions.matches(isDisplayed())).perform(clearText());
        onView(withId(R.id.etConfirmPassword)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("admin"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        Intents.init();
        intended(hasComponent(hasClassName(MainActivity.class.getName())));

//LOGIN
        //INPUT EMAIL
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(withText("Login")));
        onView(withId(R.id.etEmail)).check(ViewAssertions.matches(isDisplayed())).perform(typeText(""));
        onView(withId(R.id.etPassword)).check(ViewAssertions.matches(isDisplayed())).perform(typeText(""));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        onView(withId(R.id.etEmail)).check(ViewAssertions.matches(isDisplayed())).perform(clearText());
        onView(withId(R.id.etEmail)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("admin"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        //INPUT EMAIL WITH PASSWORD
        onView(withId(R.id.etEmail)).check(ViewAssertions.matches(isDisplayed())).perform(clearText());
        onView(withId(R.id.etEmail)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("admin@admin.com"));
        onView(withId(R.id.etPassword)).check(ViewAssertions.matches(isDisplayed())).perform(replaceText(""));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        onView(withId(R.id.etPassword)).check(ViewAssertions.matches(isDisplayed())).perform(clearText());
        onView(withId(R.id.etPassword)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("password"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        onView(withId(R.id.etPassword)).check(ViewAssertions.matches(isDisplayed())).perform(clearText());
        onView(withId(R.id.etPassword)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("admin"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

//RECYCLER VIEW
        //SCROLL TO INDEX
        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(100));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(0));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(100));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(0));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(27));

        //CLICK ON ITEM AT INDEX
        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.actionOnItemAtPosition(27, click()));

//GRID VIEW
        //SCROLL TO INDEX
        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(100));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(200));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(300));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(400));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(500));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(600));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(700));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(800));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(900));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(1000));

        //CLICK ON ITEM AT INDEX
        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.actionOnItemAtPosition(50, click()));

//VIEWPAGER
        //SWIPE ON VIEWPAGER
        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeRight());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeRight());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeLeft());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeLeft());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeRight());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeRight());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeLeft());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeLeft());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeRight());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeRight());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeLeft());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeLeft());

// SIDEDRAWER
        //OPENING AND CLOSING SIDE DRAWER
        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.open());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.close());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.open());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.close());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.open());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.close());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.open());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.close());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.open());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.close());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.open());

        //SELECTING ITEM AT INDEX ON SIDE DRAWER
        onView(withId(R.id.navDrawerList))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        Thread backpress = new Thread() {
            @Override
            public void run() {
                try {
                    super.run();
                    sleep(5000);
                } catch (Exception e) {
                    Log.e("error", e.getMessage());
                } finally {
                    //RETURN TO PREVIOUS SCREEN
                    onView(withId(R.id.recycler_view))
                            .check(ViewAssertions.matches(isDisplayed()))
                            .perform(pressBack());
                }
            }
        };
        backpress.start();
    }
}
