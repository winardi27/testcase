package com.win.testcase;

import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by Winardi on 8/4/17.
 */

public class RecyclerViewTest {
    @Rule
    public ActivityTestRule<RecyclerViewActivity> mActivity = new ActivityTestRule<>(RecyclerViewActivity.class);

    @Test
    public void clickatPosition() {

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(100));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(200));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(300));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(400));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(500));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(600));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(700));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(800));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(900));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(1000));

        onView(withId(R.id.recycler_view))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.actionOnItemAtPosition(50, click()));
    }
}
