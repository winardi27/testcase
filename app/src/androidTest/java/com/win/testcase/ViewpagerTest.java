package com.win.testcase;

import android.Manifest;
import android.os.Build;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.util.Log;

import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressBack;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by Winardi on 8/4/17.
 */

public class ViewpagerTest {
    @Rule
    public ActivityTestRule<ViewpagerActivity> mActivity = new ActivityTestRule<>(ViewpagerActivity.class);

    @Test
    public void clickatPosition() {

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeRight());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeRight());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeLeft());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeLeft());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeRight());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeRight());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeLeft());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeLeft());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeRight());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeRight());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeLeft());

        onView(withId(R.id.viewPager))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(swipeLeft());

        // OPEN DRAWER WITH CLICK
        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(click());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(click());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.open());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.close());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.open());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.close());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.open());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.close());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.open());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.close());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.open());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.close());

        onView(withId(R.id.mDrawerLayout))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(DrawerActions.open());

        onView(withId(R.id.navDrawerList))
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        Thread thread1 = new Thread() {
            @Override
            public void run() {
                try {
                    super.run();
                    sleep(5000);
                } catch (Exception e) {
                    Log.e("error", e.getMessage());
                } finally {
                    onView(withId(R.id.recycler_view))
                            .check(ViewAssertions.matches(isDisplayed()))
                            .perform(pressBack());
                }
            }
        };
        thread1.start();

        Thread sideDrawer1 = new Thread() {
            @Override
            public void run() {
                try {
                    super.run();
                    sleep(5000);
                } catch (Exception e) {
                    Log.e("error", e.getMessage());
                } finally {
                    onView(withId(R.id.navDrawerList))
                            .check(ViewAssertions.matches(isDisplayed()))
                            .perform(RecyclerViewActions.actionOnItemAtPosition(1, click()));
                }
            }
        };
        sideDrawer1.start();

        Thread thread2 = new Thread() {
            @Override
            public void run() {
                try {
                    super.run();
                    sleep(5000);
                } catch (Exception e) {
                    Log.e("error", e.getMessage());
                } finally {
                    onView(withId(R.id.recycler_view))
                            .check(ViewAssertions.matches(isDisplayed()))
                            .perform(pressBack());
                }
            }
        };
        thread2.start();

        Thread sideDrawer2 = new Thread() {
            @Override
            public void run() {
                try {
                    super.run();
                    sleep(5000);
                } catch (Exception e) {
                    Log.e("error", e.getMessage());
                } finally {
                    onView(withId(R.id.navDrawerList))
                            .check(ViewAssertions.matches(isDisplayed()))
                            .perform(RecyclerViewActions.actionOnItemAtPosition(1, click()));
                }
            }
        };
        sideDrawer2.start();

        Thread thread3 = new Thread() {
            @Override
            public void run() {
                try {
                    super.run();
                    sleep(5000);
                } catch (Exception e) {
                    Log.e("error", e.getMessage());
                } finally {
                    onView(withId(R.id.etEmail))
                            .check(ViewAssertions.matches(isDisplayed()))
                            .perform(pressBack());
                }
            }
        };
        thread3.start();
    }
}
