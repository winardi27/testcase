package com.win.testcase;

import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.times;
import static android.support.test.espresso.intent.matcher.ComponentNameMatchers.hasClassName;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by tommygoh on 4/13/17.
 */


@RunWith(AndroidJUnit4.class)
public class IntentTest {
    @Rule
    public IntentsTestRule<RegisterActivity> mActivity = new IntentsTestRule<>(RegisterActivity.class);

    @Test
    public void clickatPosition() {
        onView(withId(R.id.etFirstName)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("Hi"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        onView(withId(R.id.etFirstName)).check(ViewAssertions.matches(isDisplayed())).perform(clearText());
        onView(withId(R.id.etFirstName)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("Cat"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        //INPUT LAST NAME
        onView(withId(R.id.etLastName)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("Yo"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        onView(withId(R.id.etLastName)).check(ViewAssertions.matches(isDisplayed())).perform(clearText());
        onView(withId(R.id.etLastName)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("Dog"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        //INPUT EMAIL
        onView(withId(R.id.etEmail)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("test"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        onView(withId(R.id.etEmail)).check(ViewAssertions.matches(isDisplayed())).perform(clearText());
        onView(withId(R.id.etEmail)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("admin@admin.com"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        //INPUT PASSWORD
        onView(withId(R.id.etPassword)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("test"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        onView(withId(R.id.etPassword)).check(ViewAssertions.matches(isDisplayed())).perform(clearText());
        onView(withId(R.id.etPassword)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("admin"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        //INPUT CONFIRM PASSWORD
        onView(withId(R.id.etConfirmPassword)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("test"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        onView(withId(R.id.etConfirmPassword)).check(ViewAssertions.matches(isDisplayed())).perform(clearText());
        onView(withId(R.id.etConfirmPassword)).check(ViewAssertions.matches(isDisplayed())).perform(typeText("admin"));
        onView(withId(R.id.btnSubmit)).check(ViewAssertions.matches(isDisplayed())).perform(click());

        intended(hasComponent(hasClassName(MainActivity.class.getName())));
    }
}
