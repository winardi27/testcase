package com.win.testcase;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.EditText;
import android.widget.Toast;

import com.win.testcase.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etPassword)
    EditText etPassword;

    private String savedEmail;
    private String savedPassword;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void onViewCreated() {
        SharedPreferences sharedPref = this.getSharedPreferences("testcase", Context.MODE_PRIVATE);
        savedEmail = sharedPref.getString("email", "");
        savedPassword = sharedPref.getString("password", "");
    }

    @OnClick(R.id.btnSubmit)
    void btnSubmit() {
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();

        if (email.isEmpty() && password.isEmpty()) {
            Toast.makeText(MainActivity.this, "Please enter your email and password", Toast.LENGTH_SHORT).show();
        } else if (email.isEmpty()) {
            Toast.makeText(MainActivity.this, "Please enter your email", Toast.LENGTH_SHORT).show();
        } else if (password.isEmpty()) {
            Toast.makeText(MainActivity.this, "Please enter your password", Toast.LENGTH_SHORT).show();
        } else if (!email.matches(savedEmail)) {
            Toast.makeText(MainActivity.this, "This email is not registered", Toast.LENGTH_SHORT).show();
        } else if (email.matches(savedEmail) && !password.matches(savedPassword)) {
            Toast.makeText(MainActivity.this, "Wrong password", Toast.LENGTH_SHORT).show();
        } else if (email.matches(savedEmail) && password.matches(savedPassword)) {
            startActivity(new Intent(MainActivity.this, GridViewActivity.class));
        }
    }
}
