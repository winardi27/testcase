package com.win.testcase;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.EditText;

import com.win.testcase.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity {

    @BindView(R.id.etFirstName)
    EditText etFirstName;
    @BindView(R.id.etLastName)
    EditText etLastName;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.etConfirmPassword)
    EditText etConfirmPassword;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_register;
    }

    @Override
    protected void onViewCreated() {

    }

    @OnClick(R.id.btnSubmit)
    void btnSubmit() {
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();
        boolean validationSuccess = true;

        if (firstName.isEmpty()) {
            validationSuccess = false;
            etFirstName.setError("Please enter your first name");
        } else if (firstName.length() < 3) {
            etFirstName.setError("First name must be higher than 3 characters");
        } else {
            etFirstName.setError(null);
        }

        if (lastName.isEmpty()) {
            validationSuccess = false;
            etLastName.setError("Please enter your last name");
        } else if (lastName.length() < 3) {
            etLastName.setError("Last name must be higher than 3 characters");
        } else {
            etLastName.setError(null);
        }

        if (email.isEmpty()) {
            validationSuccess = false;
            etEmail.setError("Please enter your email");
        } else if (!isValidEmail(email)) {
            etEmail.setError("Please enter a valid email");
        } else {
            etEmail.setError(null);
        }

        if (password.isEmpty()) {
            validationSuccess = false;
            etPassword.setError("Please enter your password");
        } else if (password.length() < 5 || password.length() > 20) {
            etPassword.setError("Password must be between 5 - 20 characters");
        } else {
            etPassword.setError(null);
        }

        if (confirmPassword.length() == 0 || !confirmPassword.matches(password)) {
            validationSuccess = false;
            etConfirmPassword.setError("Your password does not match");
        } else {
            etConfirmPassword.setError(null);
        }

        if (validationSuccess) {
            SharedPreferences sharedPref = this.getSharedPreferences("testcase", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("email", email);
            editor.putString("password", password);
            editor.apply();
            startActivity(new Intent(RegisterActivity.this, MainActivity.class));
        }
    }

    private static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}
