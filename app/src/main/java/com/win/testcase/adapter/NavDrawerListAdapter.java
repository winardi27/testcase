package com.win.testcase.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.win.testcase.DrawerItem;
import com.win.testcase.R;
import com.win.testcase.base.BaseAdapter;

import butterknife.BindView;

public class NavDrawerListAdapter extends BaseAdapter<DrawerItem, NavDrawerListAdapter.ViewHolder> {
    private final OnItemClickListener mListener;
    public NavDrawerListAdapter(OnItemClickListener listener) {
        this.mListener = listener;
    }

    @Override
    protected ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_drawer_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        DrawerItem drawerItem = getItem(viewHolder.getAdapterPosition());

        viewHolder.title.setText(drawerItem.getTitle());
        viewHolder.icon.setImageResource(drawerItem.getIcon());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClick(v, viewHolder.getAdapterPosition());
            }
        });
    }

    public interface OnItemClickListener {
        void onClick(View view, int position);
    }

    public class ViewHolder extends BaseAdapter.BaseViewHolder{
        @BindView(R.id.icon) ImageView icon;
        @BindView(R.id.title) TextView title;
        public ViewHolder(View view) {
            super(view);
        }
    }
}