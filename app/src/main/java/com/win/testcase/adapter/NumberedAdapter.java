package com.win.testcase.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.win.testcase.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class NumberedAdapter extends RecyclerView.Adapter<ButtonHolder> {
    private final OnItemClickListener listener;
    private List<String> labels;

    public NumberedAdapter(int count, OnItemClickListener listener) {
        labels = new ArrayList<>(count);
        for (int i = 0; i < count; ++i) {
            labels.add(String.valueOf(i + 1));
        }
        this.listener = listener;
    }

    @Override
    public ButtonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_recycler_view, parent, false);
        return new ButtonHolder(view);
    }

    @Override
    public void onBindViewHolder(final ButtonHolder holder, int position) {
        Random random = new Random();
        final String label = labels.get(position);
        holder.button.setText(label);
        holder.button.setBackgroundColor(Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256)));

        if (listener != null) {
            holder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(holder.getAdapterPosition());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return labels.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}