package com.win.testcase.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

public class ButtonHolder extends RecyclerView.ViewHolder {
    public Button button;

    public ButtonHolder(View itemView) {
        super(itemView);
        button = (Button) itemView;
    }
}