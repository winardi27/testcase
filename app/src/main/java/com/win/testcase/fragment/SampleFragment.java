package com.win.testcase.fragment;

import android.graphics.Color;
import android.widget.TextView;

import com.win.testcase.R;
import com.win.testcase.base.BaseFragment;

import java.util.Random;

import butterknife.BindView;

/**
 * Created by Winardi on 9/4/17.
 */

public class SampleFragment extends BaseFragment {

    @BindView(R.id.tvFragment)
    TextView tvFragment;

    @Override
    protected int getContentViewResource() {
        return R.layout.sample_fragment;
    }

    @Override
    protected void onViewCreated() {
        Random random = new Random();
        tvFragment.setBackgroundColor(Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256)));
        tvFragment.setTextColor(Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256)));
    }

}
