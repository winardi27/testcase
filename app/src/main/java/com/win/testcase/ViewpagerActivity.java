package com.win.testcase;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.win.testcase.adapter.NavDrawerListAdapter;
import com.win.testcase.adapter.ViewPagerAdapter;
import com.win.testcase.base.ToolbarActivity;
import com.win.testcase.fragment.SampleFragment;
import com.win.testcase.fragment.SampleFragment2;
import com.win.testcase.fragment.SampleFragment3;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ViewpagerActivity extends ToolbarActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.mDrawerLayout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.navDrawer)
    LinearLayout navDrawer;
    @BindView(R.id.navDrawerList)
    RecyclerView navDrawerList;

    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_viewpager;
    }

    @Override
    protected void onViewCreated() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, this.toolbar, R.string.app_name, R.string.app_name);
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        navDrawerList.setHasFixedSize(true);
        RecyclerView.LayoutManager drawerLayoutManager = new LinearLayoutManager(getApplicationContext());
        navDrawerList.setLayoutManager(drawerLayoutManager);

        List<DrawerItem> sideMenuList = new ArrayList<>();
        sideMenuList.add(new DrawerItem("RECYLERVIEW", R.mipmap.ic_launcher));
        sideMenuList.add(new DrawerItem("GRIDVIEW", R.mipmap.ic_launcher_round));
        sideMenuList.add(new DrawerItem("LOGIN", R.mipmap.ic_launcher_round));

        NavDrawerListAdapter navDrawerListAdapter = new NavDrawerListAdapter(new NavDrawerListAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                mDrawerLayout.closeDrawer(navDrawer);
                switch (position) {
                    case 0:
                        Intent chatIntent = new Intent(ViewpagerActivity.this, RecyclerViewActivity.class);
                        startActivity(chatIntent);
                        break;
                    case 1:
                        Intent settingsIntent = new Intent(ViewpagerActivity.this, GridViewActivity.class);
                        startActivity(settingsIntent);
                        break;
                    case 2:
                        Intent feedbackIntent = new Intent(ViewpagerActivity.this, MainActivity.class);
                        startActivity(feedbackIntent);
                        break;
                    default:
                        break;
                }
            }
        });
        navDrawerList.setAdapter(navDrawerListAdapter);
        navDrawerListAdapter.setItems(sideMenuList);

        ViewPagerAdapter mAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        mAdapter.addPage(new SampleFragment(), "FIRST");
        mAdapter.addPage(new SampleFragment2(), "SECOND");
        mAdapter.addPage(new SampleFragment3(), "THIRD");
        viewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(ViewpagerActivity.this, R.color.colorAccent));
    }
}
