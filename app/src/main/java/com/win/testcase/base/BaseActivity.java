package com.win.testcase.base;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by tomykho on 14/6/16.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    @Deprecated
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        int contentViewResource = getContentViewResource();
        if (contentViewResource != 0) setContentView(getContentViewResource());
        onBindView();
        onViewCreated();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    protected void onBindView() {
        ButterKnife.bind(this);
    }

    @LayoutRes
    protected abstract int getContentViewResource();

    protected abstract void onViewCreated();

}
