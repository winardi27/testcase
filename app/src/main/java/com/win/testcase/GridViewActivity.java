package com.win.testcase;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.win.testcase.adapter.NumberedAdapter;
import com.win.testcase.base.BaseActivity;
import com.win.testcase.base.ToolbarActivity;

import butterknife.BindView;

public class GridViewActivity extends ToolbarActivity {
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_grid_view;
    }

    @Override
    protected void onViewCreated() {
        recycler_view.setHasFixedSize(true);
        recycler_view.setLayoutManager(new GridLayoutManager(this, 2));
        recycler_view.setAdapter(new NumberedAdapter(1000, new NumberedAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                startActivity(new Intent(GridViewActivity.this, ViewpagerActivity.class));
            }
        }));
    }
}
