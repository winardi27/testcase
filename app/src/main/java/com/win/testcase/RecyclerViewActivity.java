package com.win.testcase;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.win.testcase.adapter.NumberedAdapter;
import com.win.testcase.base.BaseActivity;
import com.win.testcase.base.ToolbarActivity;

import butterknife.BindView;

public class RecyclerViewActivity extends ToolbarActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_recycler_view;
    }

    @Override
    protected void onViewCreated() {
        recycler_view.setHasFixedSize(true);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setAdapter(new NumberedAdapter(1000, new NumberedAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Toast.makeText(RecyclerViewActivity.this, "Selected " + position, Toast.LENGTH_SHORT).show();
                startActivity(new Intent(RecyclerViewActivity.this, GridViewActivity.class));
            }
        }));
    }
}
